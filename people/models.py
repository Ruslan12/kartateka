# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.core import validators
# Create your models here.

class AuthManager(BaseUserManager):
    def create_user(self, login, email, password=None):
 
        if not login:
            raise ValueError(_("User must have an username"))
        user = self.model(login=login, email=self.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, login, email, password):
        user = self.model(login=login, email=self.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class Users(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """

    FEMALE = 0
    MALE = 1
    GENDER_CHOICE = ((FEMALE, _("female")), (MALE, _("male")))

    login = models.CharField(_('login'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[validators.RegexValidator(r'^[\w.@+-]+$',
                                            _('Enter a valid username. '
                                              'This value may contain only letters, numbers '
                                              'and @/./+/-/_ characters.'), 'invalid')],
                                error_messages={
                                    'unique': _("A user with that username already exists.")})

    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    middle_name = models.CharField(_('middle name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    phone = models.CharField(_('phone'), max_length=40, blank=True, null=True)
    phone_additional = models.CharField(_('Additional phone'), max_length=40, blank=True, null=True)
    gender = models.PositiveSmallIntegerField(_('gender'), choices=GENDER_CHOICE, default=MALE)
    birth_date = models.DateField(_('Birth date'), blank=True, null=True)

    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    passport_series = models.CharField(_('Passport series'), max_length=3, blank=True, null=True)
    passport_number = models.CharField(_('Passport number'), max_length=20, blank=True, null=True)

    objects = AuthManager()

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        """
        Returns the middle_name plus first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.last_name, self.first_name, self.middle_name)
        return full_name.strip()

    def get_short_name(self):
        # Returns the short name for the user.
        return '%s %s %s' % ((self.last_name or ""),
                               (self.first_name[0]+"." if len(self.first_name or "") > 0 else ""),
                               (self.middle_name[0]+"." if len(self.middle_name or "") > 0 else ""))

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

 

