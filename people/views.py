from django.shortcuts import render

# Create your views here.
from django.contrib.auth.decorators import permission_required, login_required

@login_required
def people_add(request):
  if request.user.is_superuser:
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('place_address_list')
    else:
        form = UserForm()

    return render(request, 'people/add.html', {"form": form})
  else:
    messages.info(request,_("Not access this operation"))
    return redirect(reverse('index'))
