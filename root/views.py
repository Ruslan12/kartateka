# -*- coding: utf-8 -*-


from django.contrib.auth.decorators import login_required
from django.http.response import Http404
from django.shortcuts import render
from django.contrib import messages
from people.models import  Users

@login_required
def index_view(request):

    user = request.user

    if user.is_authenticated():
        return render(request, 'root/index.html')
    raise Http404()